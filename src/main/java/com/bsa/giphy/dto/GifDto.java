package com.bsa.giphy.dto;

import com.bsa.giphy.entity.Gif;
import lombok.*;

@Builder
@Data
@Getter
@Setter
@AllArgsConstructor
public class GifDto {
    private String keyword;
    private String path;

    public static GifDto mapperGifToDto(Gif gif) {
        return builder()
                .keyword(gif.getKeyword())
                .path(gif.getPath())
                .build();
    }
}

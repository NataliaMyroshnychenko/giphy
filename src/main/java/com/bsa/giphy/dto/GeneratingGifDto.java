package com.bsa.giphy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GeneratingGifDto {
    private String query;
    private Boolean force;
}

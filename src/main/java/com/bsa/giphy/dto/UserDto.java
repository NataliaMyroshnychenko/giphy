package com.bsa.giphy.dto;

import com.bsa.giphy.entity.Gif;
import com.bsa.giphy.entity.History;
import com.bsa.giphy.entity.User;
import lombok.*;

import java.util.List;
import java.util.Map;

@Builder
@Data
@Getter
@Setter
@AllArgsConstructor
public class UserDto {
    private String name;
    private Map<String, List<Gif>> gifs;
    private List<History> history;

    public static UserDto mapperUserToDto(User user) {
        return builder()
                .name(user.getName())
                .gifs(user.getGifs())
                .history(user.getHistory())
                .build();
    }
}

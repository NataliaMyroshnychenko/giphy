package com.bsa.giphy.entity;

import com.bsa.giphy.dto.GifDto;
import lombok.*;

@Builder
@Data
@Getter
@Setter
@AllArgsConstructor
public class Gif {
    private String keyword;
    private String path;

    public static Gif mapperDtoToGif(GifDto gifDto) {
        return builder()
                .keyword(gifDto.getKeyword())
                .path(gifDto.getPath())
                .build();
    }

}

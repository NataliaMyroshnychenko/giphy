package com.bsa.giphy.entity;

import com.bsa.giphy.dto.GifDto;
import lombok.*;

import java.time.LocalDate;

@Builder
@Data
@Getter
@Setter
@AllArgsConstructor
public class History {
    private LocalDate date;
    private String query;
    private String gif;

}

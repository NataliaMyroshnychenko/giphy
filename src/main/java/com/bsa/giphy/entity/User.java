package com.bsa.giphy.entity;

import com.bsa.giphy.dto.UserDto;
import lombok.*;

import java.util.List;
import java.util.Map;

@Builder
@Data
@Getter
@Setter
@AllArgsConstructor
public class User {
    private String name;
    private Map<String, List<Gif>> gifs;
    private List<History> history;

    public static User mapperDtoToUser(UserDto userDto) {
        return builder()
                .name(userDto.getName())
                .gifs(userDto.getGifs())
                .history(userDto.getHistory())
                .build();
    }
}

package com.bsa.giphy.util;

import com.bsa.giphy.dto.GifDto;
import com.bsa.giphy.dto.UserDto;
import com.bsa.giphy.entity.Gif;
import com.bsa.giphy.entity.User;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component(value = "gifToDtoMapper")
public class GifToDtoMapper{

    public GifDto map(Gif gif) {
        return new GifDto(
                gif.getKeyword(),
                gif.getPath()
        );
    }

    public List<GifDto> mapCollection(List<Gif> gifs) {
        return gifs.stream().map(this::map).collect(Collectors.toList());
    }
}



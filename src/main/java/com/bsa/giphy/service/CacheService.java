package com.bsa.giphy.service;

import com.bsa.giphy.dto.GeneratingGifDto;
import com.bsa.giphy.entity.Gif;
import com.bsa.giphy.repository.CacheRepository;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.brunocvcunha.jiphy.Jiphy;
import org.brunocvcunha.jiphy.JiphyConstants;
import org.brunocvcunha.jiphy.model.JiphyGif;
import org.brunocvcunha.jiphy.model.JiphySearchResponse;
import org.brunocvcunha.jiphy.requests.JiphySearchRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CacheService {
    private final CacheRepository cacheRepository;

    @Autowired
    public CacheService(CacheRepository cacheRepository) {
        this.cacheRepository = cacheRepository;
    }

    public Map<String, ? extends List<Gif>> getAll() {
        return cacheRepository.getAll();
    }

    public String getAllWithoutKeyword() {

        List<Gif> gifs = cacheRepository.getAllWithoutKeyword();

        return gifs.stream().map(gif -> gif.getPath()).collect(Collectors.joining());
    }

    public Optional<Gif> getGif(String id, GeneratingGifDto generatingGifDto) {
        return cacheRepository.findByKeyword(generatingGifDto.getQuery());
    }

    public void deleteAllCache() {
        cacheRepository.deleteCache();
    }


    public Map<String, List<Gif>> getByQuery(String query) {
        return cacheRepository.getByQuery(query);
    }

    @SneakyThrows
    public Optional<Gif> generateGif(String query) {
        Jiphy jiphy = Jiphy.builder()
                .apiKey(JiphyConstants.API_KEY_BETA)
                .build();

        Optional<JiphyGif> gif = findGif(jiphy, query);
        String pathInCache = downloadGifToCache(gif.get().getUrl(), gif.get().getId(), query);

        return Optional.of(new Gif(query, pathInCache));
    }

    @SneakyThrows
    public Optional<JiphyGif> findGif(Jiphy jiphy, String query) {

        JiphySearchResponse gifs = jiphy.sendRequest(new JiphySearchRequest(query));

        Optional<JiphyGif> firstGif = gifs.getData().stream().findFirst();

        return firstGif;
    }

    @SneakyThrows
    public String downloadGifToCache(String url, String name, String keyword) {
        String pathInCache = "src/main/resources/bsa_giphy/cache/";
        File dir = new File(pathInCache + "/" + keyword + "/");
        if (!dir.isDirectory()) {
            dir.mkdir();
        }
        File file = new File(dir.getPath() + name + ".gif");
        FileUtils.copyURLToFile(
                new URL(url),
                file);
        cacheRepository.save(new Gif(keyword, file.getPath()));
        return pathInCache;
    }
}

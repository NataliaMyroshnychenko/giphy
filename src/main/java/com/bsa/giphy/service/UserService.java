package com.bsa.giphy.service;

import com.bsa.giphy.entity.Gif;
import com.bsa.giphy.entity.History;
import com.bsa.giphy.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository usersFSRepository;

    @Autowired
    public UserService(UserRepository usersFSRepository) {
        this.usersFSRepository = usersFSRepository;
    }

    public Map<String, List<Gif>> getAllFilesByUser(String user, String id) {
        return usersFSRepository.getAll().get(user).getGifs();
    }

    public List<History> getHistoryByUser(String user, String id) {
        return usersFSRepository.getHistoryByUser(user, id);
    }

    public void deleteHistoryByUser(String user, String id) {
        usersFSRepository.deleteHistoryByUser(user, id);
    }

    public List<String> searchGif(String user, String id, String query) {
        return usersFSRepository.getAll()
                .get(user)
                .getGifs()
                .get(query)
                .stream()
                .map(Gif::getPath)
                .filter(path -> id.equals(parsePath(path)))
                .collect(Collectors.toList());
    }

    private String parsePath(String path) {
        return path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf("."));
    }

    public void deleteFileByUser(String user, String id) {
        usersFSRepository.deleteFileByUser(user, id);
    }

    public void deleteFilesByQuery(String user, String query, String id) {
        usersFSRepository.deleteFilesByQuery(user, query, id);

    }
}

package com.bsa.giphy.service;

public interface UsersApiClient {
    String getUsersAsJson();
}

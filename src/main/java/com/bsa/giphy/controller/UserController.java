package com.bsa.giphy.controller;

import com.bsa.giphy.entity.Gif;
import com.bsa.giphy.entity.History;
import com.bsa.giphy.service.CacheService;
import com.bsa.giphy.service.UserService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.slf4j.LoggerFactory.getLogger;

@RestController
@RequestMapping("/")
public class UserController {

    private final CacheService cacheService;
    private final UserService userService;
    private static final Logger log = getLogger(UserController.class);

    @Autowired
    public UserController(CacheService cacheService, UserService userService) {
        this.cacheService = cacheService;
        this.userService = userService;
    }

    @GetMapping(path = "{user}/{id}/all")
    public Map<String, List<Gif>> getAllFilesByUser(@PathVariable String user, @PathVariable String id) {
        log.info("--- method getAllFilesByUser with param user={}, id={}", user, id);
        return userService.getAllFilesByUser(user, id);
    }

    @GetMapping(path = "{user}/{id}/history")
    public List<History> getHistoryByUser(@PathVariable String user, @PathVariable String id) {
        log.info("--- method getHistoryByUser with param user={}, id={}", user, id);

        return userService.getHistoryByUser(user, id);
    }

    @DeleteMapping(path = "{user}/{id}/history/clean")
    public void deleteHistoryByUser(@PathVariable String user, @PathVariable String id) {
        log.info("--- method deleteHistoryByUser with param user={}, id={}", user, id);
        log.info("--- method deleteHistoryByUser getHistory={}", userService.getHistoryByUser(user, id).toString());

        userService.deleteHistoryByUser(user, id);
    }

    @GetMapping(path = "{user}/{id}/search")
    public List<String> searchGif(@PathVariable String user, @PathVariable String id, @RequestParam String query, @RequestParam(required = false) Boolean force) {
        log.info("--- method searchGif with param user={}, id={}, query={}, force={}", user, id, query, force);

        List<String> pathGifs = new ArrayList<>();

        if (force == null || !force) {

            pathGifs = cacheService.getByQuery(query).get(query)
                    .stream().map(Gif::getPath).collect(Collectors.toList());
        } else {
            pathGifs = userService.searchGif(user, id, query);
        }
        log.info("--- method searchGif pathGifs={}", pathGifs);

        return pathGifs;
    }

    @DeleteMapping(path = "{user}/{id}/reset")
    public void deleteFilesByQuery(@PathVariable String user, @PathVariable String id, @RequestParam(required = false) String query) {
        log.info("--- method deleteFilesByQuery with param user={}, id={}, query={}", user, id, query);

        if (query == null || query.isEmpty()) {
            userService.deleteFileByUser(user, id);
        } else {
            userService.deleteFilesByQuery(user, query, id);
        }
    }
}



package com.bsa.giphy.controller;

import com.bsa.giphy.entity.Gif;
import com.bsa.giphy.service.CacheService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.slf4j.LoggerFactory.getLogger;

@RestController
@RequestMapping("/")
public class CacheController {
    private final CacheService cacheService;
    private static final Logger log = getLogger(CacheController.class);

    @Autowired
    public CacheController(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @GetMapping(path = "/cache")
    public Map<String, List<Gif>> getCache(@RequestParam(required = false) String query) {
        log.info("--- method getCache with param query={}", query);

        Map<String, List<Gif>> response = new HashMap<>();
        if (query.isEmpty()) {
            response.putAll(cacheService.getAll());
        } else {
            response.putAll(cacheService.getByQuery(query));
        }
        log.info("--- method getCache response={}", response);

        return response;
    }

    @PostMapping(path = "/cache/generate")
    public Map<String, List<Gif>> generateGif(@RequestParam String query) {
        log.info("--- method generateGif with param query={}", query);

        cacheService.generateGif(query);
        log.info("--- method generateGif return={}", cacheService.getByQuery(query));

        return cacheService.getByQuery(query);
    }

    @DeleteMapping(path = "/cache")
    public void deleteAllCache() {
        log.info("--- method deleteAllCache");

        cacheService.deleteAllCache();
    }

}

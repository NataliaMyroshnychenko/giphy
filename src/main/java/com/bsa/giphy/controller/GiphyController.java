package com.bsa.giphy.controller;

import com.bsa.giphy.dto.GeneratingGifDto;
import com.bsa.giphy.entity.Gif;
import com.bsa.giphy.service.CacheService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static org.slf4j.LoggerFactory.getLogger;

@RestController
@RequestMapping("/")
public class GiphyController {
    private final CacheService cacheService;
    private static final Logger log = getLogger(GiphyController.class);

    @Autowired
    public GiphyController(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @PostMapping(path = "{user}/{id}/generate")
    public String generateGif(@PathVariable String user, @PathVariable String id, @RequestParam String query, @RequestParam(required = false) Boolean force) {
        log.info("--- method generateGif with param user={}, id={}, query={}, force={}", user, id, query, force);
        GeneratingGifDto generatingGifDto = new GeneratingGifDto(query, force);
        Optional<Gif> gif = cacheService.getGif(id, generatingGifDto);
        log.info("--- method generateGif gif={}", gif);

        if (gif.isEmpty()) {
            gif = cacheService.generateGif(query);
        }
        return gif.get().getPath();
    }

    @GetMapping(path = "/gifs")
    public String getAllFilesWithoutKeywords() {
        String allWithoutKeyword = cacheService.getAllWithoutKeyword();
        log.info("--- method getAllFilesWithoutKeywords ={}", allWithoutKeyword);
        return allWithoutKeyword;
    }


}

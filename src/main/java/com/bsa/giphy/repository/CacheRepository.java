package com.bsa.giphy.repository;

import com.bsa.giphy.entity.Gif;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.util.*;

@Repository
public class CacheRepository {
    private static final Map<String, List<Gif>> cacheMap = new HashMap<>();
    private final File cacheStorage;


    public CacheRepository(@Value("${bsa_giphy.cache}") String path) {
        cacheStorage = new File(path);
    }

    public Map<String, List<Gif>> getAll() {
        return cacheMap;
    }

    public Map<String, List<Gif>> getByQuery(String query) {
        List<Gif> gifs = cacheMap.get(query);
        HashMap<String, List<Gif>> result = new HashMap<>();
        result.put(query, gifs);
        return result;
    }

    public List<Gif> getAllWithoutKeyword() {
        return new ArrayList(cacheMap.values());
    }

    public Optional<Gif> findByKeyword(String keyword) {
        return cacheMap.get(keyword).stream().findFirst();
    }

    public void save(Gif gif) {
        String keyword = gif.getKeyword();

        if (cacheMap.containsKey(keyword)) {
            List<Gif> gifs = cacheMap.get(keyword);
            gifs.add(gif);
            cacheMap.put(keyword, gifs);

        } else {
            cacheMap.put(keyword, List.of(gif));
        }
    }

    public void deleteCache() {

        cacheMap.clear();

        for (File file : Objects.requireNonNull(cacheStorage.listFiles()))
            if (file.isFile()) file.delete();
    }


}

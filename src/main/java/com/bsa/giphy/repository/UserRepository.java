package com.bsa.giphy.repository;

import com.bsa.giphy.entity.Gif;
import com.bsa.giphy.entity.History;
import com.bsa.giphy.entity.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.util.*;

@Repository
public class UserRepository {
    private static final Map<String, User> users = new HashMap<>();
    private final File usersStorage;

    public UserRepository(@Value("${bsa_giphy.users}") String path) {
        usersStorage = new File(path);
    }

    public Map<String, User> getAll() {
        return users;
    }

    public List<History> getHistoryByUser(String user, String id) {
        return users.get(user).getHistory();
    }

    public void deleteHistoryByUser(String user, String id) {
        users.get(user).setHistory(new ArrayList<>());
    }

    public void deleteFileByUser(String userName, String id) {
        User user = users.get(userName);
        user.setGifs(new HashMap<String, List<Gif>>());
        users.put(userName, user);
    }

    public void deleteFilesByQuery(String userName, String query, String id) {
        User user = users.get(userName);
        Map<String, List<Gif>> gifs = user.getGifs();
        List<Gif> gifsByQuery = gifs.get(query);
        deleteFilesByQueryInFS(gifsByQuery.stream()
                .filter(gif -> {
                    String parsingPath = gif.getPath().substring(gif.getPath().lastIndexOf("\\") + 1, gif.getPath().lastIndexOf("."));
                    return id.equals(parsingPath);
                })
                .map(Gif::getPath)
                .findFirst()
                .get());
        gifsByQuery.remove(id);
        gifs.put(query, gifsByQuery);
        user.setGifs(gifs);
        users.put(userName, user);
    }

    public void deleteFilesByQueryInFS(String path) {
        File file = new File(path);
        file.delete();
    }
}
